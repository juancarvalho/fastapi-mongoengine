FROM python:3.8

COPY . /api

RUN pip install --require-hashes -r /api/requirements.txt

# CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
import unittest
from urllib.parse import urljoin
import requests

# URLs ===========================================
url_server = "http://localhost:8000/api/v1/"
get_all_students_url = "get-students"
get_students_by_id_url = "student"
create_student_url = "create-student"
update_students_by_id_url = "update-student"
delete_student_by_id_url = "delete-student"

# Temp Vars ======================================
student_id = "5f49241c81fd82b81c22f44f"
username = "Marquinho"
password = "Marquinho123"
status = "active"
classroom = "second"
grade = "high school"
ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.\
eyJ1c2VybmFtZSI6Ikp1YW5DYXJ2YWxobyIsImV4cCI6MTU5ODYzMjMzNX0.\
65qTmVcASJ2m0dCpGfkuOQaVrmpOPFAhijmIFM1zvGM"

# TODO: Implement Mocking Objects


class TestStudents(unittest.TestCase):

    def test_a_get_all_students(self):
        url = urljoin(url_server, get_all_students_url)
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.get(url, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_b_get_student(self):
        url = urljoin(url_server, get_students_by_id_url) + "/" + student_id
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.get(url, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_c_create_student(self):
        url = urljoin(url_server, create_student_url)
        data = {
            "username": username,
            "password": password,
            "status": status,
            "classroom": classroom,
            "grade": grade,
            }
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.post(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_d_update_student(self):
        url = urljoin(url_server, update_students_by_id_url)
        data = {
            "id": student_id,
            "username": "Joao",
            "status": status,
            "classroom": classroom,
            "grade": grade,
            }
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.put(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_e_delete_student(self):
        url = urljoin(url_server, delete_student_by_id_url) + "/" + student_id
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.delete(url, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)


if __name__ == "__main__":
    unittest.main()

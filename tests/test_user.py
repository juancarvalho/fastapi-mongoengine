import unittest
from urllib.parse import urljoin
import requests
import json

# URLs ===========================================
url_server = "http://localhost:8000/api/v1/"
status_url = "status-server"
create_user_url = "create-user"
delete_user_url = "delete-user"
update_user_url = "update-user"
get_users_url = "get-users"
get_user_url = "user"
auth_user_url = "auth"

# Temp Vars ======================================
user_id = "5f484ea62ce3fe27debe73a9"
user = "juan"
password = "12345abc"
status = "active"
ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.\
eyJ1c2VybmFtZSI6Ikp1YW5DYXJ2YWxobyIsImV4cCI6MTU5ODYzMjMzNX0.\
65qTmVcASJ2m0dCpGfkuOQaVrmpOPFAhijmIFM1zvGM"

# TODO: Implement Mocking Objects


class TestUsers(unittest.TestCase):

    def test_a_auth(self):
        url = urljoin(url_server, auth_user_url)
        data = {"username": "JuanCarvalho", "password": "1234"}
        response = requests.post(url, json=data)
        print(response.json())
        self.access_token = response.json()["access_token"]
        self.assertEqual(200, response.status_code)

    def test_b_create_user(self):
        url = urljoin(url_server, create_user_url)
        data = {
            "username": user,
            "password": password,
            "status": status
            }
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.post(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_c_update_user(self):
        url = urljoin(url_server, update_user_url)
        data = {"username": user, "status": status}
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.put(url, json=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_d_delete_user(self):
        url = urljoin(url_server, delete_user_url)
        data = {"username": user}
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.delete(url, params=data, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_e_get_all_users(self):
        url = urljoin(url_server, get_users_url)
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.get(url, headers=headers)
        print(json.loads(response.json()))
        self.assertEqual(200, response.status_code)

    def test_f_get_user_by_id(self):
        url = urljoin(url_server, get_user_url) + "/" + user_id
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.get(url, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_status(self):
        url = urljoin(url_server, status_url)
        headers = {"Authorization": "Bearer {}".format(ACCESS_TOKEN)}
        response = requests.get(url, headers=headers)
        print(response.json())
        self.assertEqual(200, response.status_code)


if __name__ == "__main__":
    unittest.main()

# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""
TODO: Eliminar esse Schema Modelo e utilizar apenas as classes do mongoengine
      no retorno das requisições
      Ex.:
        @app.post("/append-usuario", response_model=UsuarioMongoEngine)
        def adiciona_usuario(usuario: UsuarioMongoEngine):
            return usuario
"""

from pydantic import BaseModel
from enum import Enum

# Models Schema ========================================


class StatusValues(str, Enum):
    """
        Choose one of the options, active or inactive
    """
    active = "active"
    inactive = "inactive"

# TODO: Criar classe Enum para tipo de usuário, Estudante, Professor ou Admin

# Create User Schema ==================================


class UserCreateInSchema(BaseModel):
    username: str
    password: str
    status: StatusValues


class UserCreateOutSchema(BaseModel):
    username: str
    status: StatusValues

# Delete User Schema ==================================


class UserDelSchema(BaseModel):
    username: str

# Update User Schema ==================================


class UserUpdateSchema(BaseModel):
    username: str
    status: StatusValues

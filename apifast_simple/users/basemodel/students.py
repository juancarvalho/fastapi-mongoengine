# !/usr/bin/env python
# coding: utf-8
from pydantic import BaseModel
from enum import Enum
from .users import UserUpdateSchema, UserCreateInSchema


class ClassRoom(str, Enum):
    """
        Choose one of the options, active or inactive
    """
    first = "first"
    second = "second"
    third = "third"


class Grade(str, Enum):
    """
        Choose one of the options, active or inactive
    """
    elementary = "elementary"
    high_school = "high school"


# Update Student Schema ===============================


class StudentUpdateSchema(UserUpdateSchema, BaseModel):
    id: str
    classroom: ClassRoom
    grade: Grade


# Update Student Schema ===============================
class StudentCreateSchema(UserCreateInSchema, BaseModel):
    classroom: ClassRoom
    grade: Grade

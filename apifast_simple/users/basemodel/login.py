# !/usr/bin/env python
# -*- coding: utf-8 -*-
from pydantic import BaseModel
from typing import Optional

# User Auth Schema ====================================


class UserAuthSchema(BaseModel):
    username: str
    password: str

# Token Schema ========================================


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None

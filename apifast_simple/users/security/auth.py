# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Library used for creating JWT token,
and checking token validity

:author: Juan Carvalho
:email: juan.carvalho82@gmail.com
"""
from datetime import datetime, timedelta
from jose import jwt, JWTError
from typing import Optional
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from ..basemodel.login import TokenData
from ..models.users import User
from mongoengine.errors import DoesNotExist

# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"

# TODO: Login via Browser/docs não fucnionou, verificar documentação
# https://fastapi.tiangolo.com/tutorial/security/
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/v1/auth")


async def validate_access_token(token: str = Depends(oauth2_scheme)):
    """
    Decode access_token and check if is valid
    :token: str

    :return ->: User [Mongoengine User]
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        # Decode received token
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        # Get payload generate on create_access_token()
        username: str = payload.get("username")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    try:
        user = User.objects.get(username=token_data.username)
    except DoesNotExist:
        raise credentials_exception
    return user


async def active_user(current_user: User = Depends(validate_access_token)):
    """
    Check if user status is active, and access_token is valid
    """
    if current_user.status == "Inactive":
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


def create_access_token(data: dict,
                        expires_delta: Optional[timedelta] = None) -> str:
    """
    TODO: Implemets two types of access tokens, by diferent users levels 

    Create a JTW Token
    :data: dict
    :expires_delta: timedelta [optional]

    :return ->: str
    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

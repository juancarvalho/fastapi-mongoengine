# !/usr/bin/env python
# coding: utf-8
"""
Library used for password manipulation

:author: Juan Carvalho
:email: juan.carvalho82@gmail.com
"""
from passlib.context import CryptContext


def hash_passwd(password: str) -> str:
    """
    Convert simple user password in hash password
    :password: str

    :return ->: str
    """
    passwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    return passwd_context.hash(password)


def check_passwd(plain_password: str, hashed_password: str) -> bool:
    """
    Compare hash password to simple password
    :password: str

    :return ->: bool
    """
    passwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    return passwd_context.verify(plain_password, hashed_password)

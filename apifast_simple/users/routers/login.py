# !/usr/bin/env python
# -*- coding: utf-8 -*-
from fastapi import APIRouter, Depends, Request
from fastapi.responses import JSONResponse
from ..models.users import User
from ..basemodel.login import UserAuthSchema
from mongoengine.errors import DoesNotExist
from ..security.security_passwd import check_passwd
from ..security.auth import create_access_token, active_user

router = APIRouter()


@router.post("/api/v1/auth", response_model=UserAuthSchema,
             tags=["Auth"])
def auth_user(user_data: UserAuthSchema):
    """
    Authenticate user by username and password\n
    and receive a access token bearer\n
    :username: str\n
    :password: str\n
    """
    try:
        # Try find user in db
        user_obj = User.objects.get(username=user_data.username)
        # Check if password is correct
        if check_passwd(plain_password=user_data.password,
                        hashed_password=user_obj.password):
            # Generate JWT access token
            token = create_access_token(data={"username": user_data.username})
            return JSONResponse(status_code=200,
                                content={"access_token": token,
                                         "token_type": "bearer"}
                                )
        else:
            return JSONResponse(status_code=401,
                                content={"msg": "Username "
                                                "or Password is wrong!"})
    except DoesNotExist:
        return JSONResponse(status_code=404,
                            content={"msg": f"User {user_data.username}"
                                            " not exists!"})

from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from ..basemodel.users import (UserCreateInSchema, UserCreateOutSchema,
                               UserUpdateSchema)
from ..models.users import User
from mongoengine.errors import NotUniqueError, DoesNotExist
from ..security.security_passwd import hash_passwd
from ..settings import token_auth, URL_API

router = APIRouter()

# Users Endpoints =============================================================


@router.post(URL_API+"/create-user", response_model=UserCreateOutSchema,
             tags=["Users"], dependencies=[Depends(token_auth)])
async def create_user(get_user: UserCreateInSchema):
    """
        Create users in database\n
        :username: string\n
        :password: string\n
        :status: string ["Active" or "Inactive"]
    """
    try:
        user_obj = User()
        user_obj.username = get_user.username
        user_obj.password = hash_passwd(get_user.password)
        user_obj.status = get_user.status.value
        user_obj.save()
        return JSONResponse(status_code=200,
                            content={"msg": f"User {get_user.username}"
                                     " created successfully!"})
    except NotUniqueError:
        return JSONResponse(status_code=404, content={"msg": "Users"
                                                      " alredy exists!"})


@router.delete(URL_API+"/delete-user", tags=["Users"],
               dependencies=[Depends(token_auth)])
async def delete_user_by_name(username: str):
    """
        Delete users in database, find by username\n
        :username: string\n
    """
    # TODO: Update function to 'delete_user_by_id()'
    try:
        user_obj = User.objects.get(username=username)
        user_obj.delete()
        return JSONResponse(status_code=200,
                            content={"msg": f"User {username}"
                                            " deleted successfully!"})
    except DoesNotExist:
        return JSONResponse(status_code=404,
                            content={"msg": f"User {username} not exists!"})


@router.put(URL_API+"/update-user", response_model=UserUpdateSchema,
            tags=["Users"], dependencies=[Depends(token_auth)])
async def update_user_by_name(get_user: UserUpdateSchema):
    """
        Update user status, find user by username\n
        :username: str\n
        :status: str [Active or Inactive]
    """
    # TODO: Update function to 'update_user_by_id()'
    try:
        user_obj = User.objects.get(username=get_user.username)
        user_obj.status = get_user.status
        user_obj.save()
        return JSONResponse(status_code=200,
                            content={"msg": f"User {get_user.username}"
                                            f" updated to {get_user.status}"
                                            " successfully!"})
    except DoesNotExist:
        return JSONResponse(status_code=404,
                            content={"msg": f"User {get_user.username}"
                                            " not exists!"})


@router.get(URL_API+"/get-users", tags=["Users"],
            dependencies=[Depends(token_auth)])
async def get_all_users():
    """
        Return all User from database\n
        \tcurl -X GET "http://localhost:8000/api/v1/get-users"
    """
    return User.objects.all().to_json()


@router.get(URL_API+"/user/{user_id}", tags=["Users"],
            dependencies=[Depends(token_auth)])
async def get_users_by_id(user_id: str):
    """
        Return User by ID \n
        exampĺe:\n
        \tcurl -X GET "http://localhost:8000/api/v1/user/5f484ea62ce3fe27debe73a9"\n
        :return ->: json
    """
    try:
        return User.objects.get(pk=user_id).to_json()
    except DoesNotExist:
        return JSONResponse(status_code=404,
                            content={"msg": f"User ID {user_id}"
                                            "does not exist!"})

# !/usr/bin/env python
# coding: utf-8
from ..settings import token_auth
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from ..models.students import Student
from ..settings import URL_API
from mongoengine.errors import DoesNotExist
from ..basemodel.students import StudentUpdateSchema, StudentCreateSchema
from ..security.security_passwd import hash_passwd

router = APIRouter()


@router.get(URL_API+"/get-students", tags=["Students"],
            dependencies=[Depends(token_auth)])
async def get_all_studens():
    """
        Return all Stunderts from database\n
        \tcurl -X GET "http://localhost:8000/api/v1/get-students"
    """
    return Student.objects.all().to_json()


@router.get(URL_API+"/student/{student_id}", tags=["Students"],
            dependencies=[Depends(token_auth)])
async def get_student_by_id(student_id: str):
    """
        Return Student by ID \n
        exampĺe:\n
        \tcurl -X GET "http://localhost:8000/api/v1/student/5f484ea62ce3fe27debe73a9"\n
        :return ->: json
    """
    try:
        return Student.objects.get(pk=student_id).to_json()
    except DoesNotExist:
        return JSONResponse(status_code=404,
                            content={"msg": f"Student ID {student_id} "
                                            "does not exist!"})


@router.post(URL_API+"/create-student", tags=["Students"],
             dependencies=[Depends(token_auth)])
async def create_student(get_student: StudentCreateSchema):
    """
        Create new student\n
        :username: str\n
        :password: str\n
        :status: str [active or inactive]\n
        :classroom: str [first or second or third]\n
        :grade: str [elementary or high school]
    """
    user_obj = Student()
    user_obj.username = get_student.username
    user_obj.password = hash_passwd(get_student.password)
    user_obj.status = get_student.status
    user_obj.classroom = get_student.classroom
    user_obj.grade = get_student.grade
    user_obj.save()
    return JSONResponse(status_code=200,
                        content={"msg": f"Student {get_student.username}"
                                        f" created successfully!"})


@router.put(URL_API+"/update-student", response_model=StudentUpdateSchema,
            tags=["Students"], dependencies=[Depends(token_auth)])
async def update_student_by_id(get_student: StudentUpdateSchema):
    """
        Update student, find student by id\n
        :id: str\n
        :username: str\n
        :status: str [active or inactive]\n
        :classroom: str [first or second or third]\n
        :grade: str [elementary or high school]
    """
    try:
        user_obj = Student.objects.get(pk=get_student.id)
        user_obj.username = get_student.username
        user_obj.status = get_student.status
        user_obj.classroom = get_student.classroom
        user_obj.grade = get_student.grade
        user_obj.save()
        return JSONResponse(status_code=200,
                            content={"msg": f"Student {get_student.id}"
                                            f" updated successfully!"})
    except DoesNotExist:
        return JSONResponse(status_code=404,
                            content={"msg": f"Student {get_student.username}"
                                            " not exists!"})


@router.delete(URL_API+"/delete-student/{user_id}", tags=["Students"],
               dependencies=[Depends(token_auth)])
async def delete_student_by_id(user_id: str):
    """
        Delete users in database, find by id\n
        :user_id: string\n
    """
    try:
        user_obj = Student.objects.get(pk=user_id)
        user_obj.delete()
        return JSONResponse(status_code=200,
                            content={"msg": f"Student {user_id}"
                                            " deleted successfully!"})
    except DoesNotExist:
        return JSONResponse(status_code=404,
                            content={"msg": f"Student {user_id} not exists!"})

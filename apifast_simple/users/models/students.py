# !/usr/bin/env python
# coding: utf-8
from .users import User
from mongoengine import Document, connect, StringField

connect(db="school", host="mongo", alias="default")

CLASSROOM = {
    "first": "First",
    "second": "Second",
    "third": "Third",
}
GRADE = {
    "elementary": "Elementary",
    "high school": "High School",
}


class Student(User):
    """
        Template for a mongoengine document, which represents a student.

        :username: unique required string value
        :password: required string value
        :status: required string value ["active" or "inactive"]
        :classroom: required string value ["first" or "second" or "third"]
        :grade: required string value ["elementary" or "high school"]

        :Example:
        >>> new_student = Student(username="juancarvalho",
                                         password="hunter2",
                                         status="active",
                                         classroom="first",
                                         grade="high school",
                                         )
        >>> new_student.save()
    """
    classroom = StringField(choices=CLASSROOM.keys(), required=True)
    grade = StringField(choices=GRADE.keys(), required=True)
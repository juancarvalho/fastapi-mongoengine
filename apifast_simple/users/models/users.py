from mongoengine import Document, connect, StringField

connect(db="school", host="mongo", alias="default")

STATUS = {
    "active": "Active",
    "inactive": "Inactive",
}

class User(Document):
    """
        Template for a mongoengine document, which represents a user.

        :username: unique required string value
        :password: required string value
        :status: required string value ["active" or "inactive"]

        :Example:
        >>> new_user = Users(username="juancarvalho",
                                       password="hunter2",
                                       status="active")
        >>> new_user.save()
    """
    # TODO: We can add email, _id, superuser, etc.
    # TODO: Convert email field to unique.
    username = StringField(required=True, max_length=200, unique=True)
    password = StringField(required=True)
    status = StringField(choices=STATUS.keys(), required=True)
    meta = {
        "allow_inheritance": True,
    }

    def __str__(self) -> str:
        return f"[<{type(self).__name__}> - {self.username}]"


if __name__ == "__main__":
    pass

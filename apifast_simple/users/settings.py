# !/usr/bin/env python
# coding: utf-8
from .security.auth import active_user

URL_API = "/api/v1"  # TODO: Remover para um arquivos settings global
tests = False  # Make True to dectivate token access

if not tests:
    token_auth = active_user  # TODO: Remover essa linha! Apenas para testes.
else:
    def test(): ...  # TODO: Remover essa linha! Apenas para testes.
    # TODO: Remover para um arquivos settings global
    token_auth = test  # TODO: Remover essa linha! Apenas para testes.

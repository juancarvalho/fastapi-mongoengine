# !/usr/bin/env python
# coding: utf-8
from fastapi import FastAPI
from users.routers import users, login, students

app = FastAPI(
    title="Test School API",
    description="Simple API to small school",
    version="0.1.0",
    test=True
)
app.include_router(users.router)
app.include_router(login.router)
app.include_router(students.router)


@app.get("/api/v1/status-server")
def status_server():
    return {"msg": "Online"}

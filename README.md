#  User API Basic [FastApi] [Mongoengine] [Docker]

### Details:
	This api was a test requested in a selection process of a company in 2020/08.
	
	author: juan.carvalho82@gmail.com
	LinkedIn: https://www.linkedin.com/in/juan-carvalho/
	Medium: https://medium.com/@juan.carvalho82
	
### Main technologies used in project:
	FastApi
	python3.8
	Docker
	MongoDB
    Poetry
	Mongoengine

### Clone repo and create containers:
```sh
$ git clone https://juancarvalho@bitbucket.org/juancarvalho/fastapi-mongoengine.git
$ cd fastapi-mongoengine
$ docker-compose build
$ docker-compose up
```

### Python Example
```python
import requests

url = "http://localhost:8000/api/v1/status-server"
response = requests.get(url)
print(response.json())
```

### Access MongoDb GUI:
	http://localhost:8081

### API Documentation:
	http://localhost:8000/docs
    http://localhost:8000/redoc

### To run tests:
```sh
$ pytest -vs --tb=line
```

### ATTENTION
    Not use this in production! It's only basic example.
    ACCESS TOKEN is disable to make tests, change it in /apifast_simple/users/settings.py